const fortunes = [
    'fortune1',
    'fortune2',
    'fortune3',
    'fortune4',
];

exports.getFortune = () => {
  const idx = Math.floor(Math.random() * fortunes.length);
  return fortunes[idx];
};

