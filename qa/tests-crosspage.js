let Browser = require('zombie'),
    assert = require('chai').assert,
    browser;

suite('All page test', () => {
   setup(() => {
     browser = new Browser();
   });
   test('Request price', (done) => {
      let referrer = 'http://localhost:3000/tours/hood-river';
      browser.visit(referrer, () => {
          browser.clickLink('requestGroupRate', () => {
             assert(browser.field('referrer').value === referrer);
             done();
          });
      })
   });
});

