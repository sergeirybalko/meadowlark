const express = require('express'),
      handlebars = require('express-handlebars'),
      fortune = require('./lib/fortunes');



let app = express();

app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.set('port', process.env.PORT || 3000);

app.use((req, res, next) => {
   res.locals.showTests = app.get('env') !== 'production' && req.query.test === '1';
   next();
});

app.get('/', (req, res) => {
   res.render('home');
});

app.get('/about', (req, res) => {
   res.render('about', {fortune: fortune.getFortune(), pageTestScript: '/qa/tests-about.js'});
});

app.get('/tours/hood-river', (req, res) => {
   res.render('tours/hood-river');
});

app.get('tours/request-group-rate', (req, res) => {
   res.render('tours/request-group-rate');
});

app.use(express.static(__dirname + '/public'));

app.use((req, res) => {
   res.status(404);
   res.render('404');
});

app.use((err, req, res, next) => {
   console.error(err.stack);
   res.status(500);
   res.render('500');
});

app.listen(app.get('port'), () => {
   console.log('Express starting on http://localhost:' + app.get('port'));
});
